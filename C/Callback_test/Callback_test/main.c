#include <stdio.h>

void call1()
{
	printf("%s\n", "call1");
}
void callback(void(*p)())
{
	(*p)();
	printf("%s\n", "callback");
}

int main()
{
	void(*p1)() = call1;
	printf("%s", "use p1 : ");
	(*p1)();

	void(*p2)() = &call1;
	printf("%s", "use p2 : ");
	(*p2)();

	callback(call1);
	callback(&call1);

	char str[] = "Hello";
	char * p3 = str;
	char * p4 = &str;
	printf("%s\n", p3);
	printf("%s\n", p4);

	char temp;
	scanf_s("%c", &temp,1);
	return 0;
}