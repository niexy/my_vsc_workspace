#include<iostream>
using namespace std;

void bubble_sort(int * arr, int size)
{
	int temp;
	bool swap;

	for (int i = size-1; i > 0; i--)
	{
		swap = false;
		for (int j = 0; j < i; j++) 
		{
			if (arr[j] < arr[j + 1])
			{
				temp = arr[j + 1];
				arr[j + 1] = arr[j];
				arr[j] = temp;
				swap = true;
			}
		}
		if (!swap) break;
	}
}

int main()
{
	int arr[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
	bubble_sort(arr, 11);
	for (int i = 0; i < 11; i++)
		cout << arr[i] << " ";
	cout << endl;
	getchar();
	return 0;
}