#include<iostream>
using namespace std;

int part(int arr[], int beg, int end)
{
	int temp = arr[beg];
	while (beg<end)
	{
		while (beg<end && arr[end]>=temp) end--;
		arr[beg] = arr[end];

		while (beg<end && arr[beg]<=temp) beg++;
		arr[end] = arr[beg];
	}

	arr[end] = temp;
	return end;
}

void quick_sort(int arr[], int beg, int end)
{
	if (beg < end) 
	{
		int pivot = part(arr, beg, end);
		quick_sort(arr, beg, pivot - 1);
		quick_sort(arr, pivot + 1, end);
	}
}

int main()
{
	int arr[] = { 12, 2, 4, 21, 6, 12, 2, 34, 32, 9, 10, 34, 0, 11 };
	int beg = 0;
	int end = sizeof(arr) / sizeof(int) - 1;
	quick_sort(arr, beg, end);

	for (int i = beg; i <= end; i++)
	{
		cout << arr[i] << ' ';
	}
	cout << endl;

	getchar();
	return 0;
}