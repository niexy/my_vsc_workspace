#include<iostream>
using namespace std;

void insert_sort(int arr[],int begin, int end)
{
	int temp;
	int i, j;

	for (i = begin+1; i <= end; i++)
	{
		temp = arr[i];
		if (temp < arr[i - 1])
		{
			for (j = i - 1; temp < arr[j] && j >= begin; j--)
			{
				arr[j + 1] = arr[j];
			}
			j++;
			arr[j] = temp;
		}
	}
}

int main() 
{
	int arr[] = {3, 1, 2, 77, 34, 2, 23, 9, 30, 25 };
	int begin = 0;
	int end = sizeof(arr) / sizeof(int) - 1;
	insert_sort(arr, begin, end);

	for (int i = 0; i <= end; i++)
		cout << arr[i] << ' ';
	cout << endl;

	getchar();
	return 0;
}