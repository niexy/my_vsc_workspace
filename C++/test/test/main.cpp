﻿#include<iostream>
#include<cmath>
using namespace std;

int main()
{
	char matrix[3][3];
	int cntx, cnt0, cnt_;
	int wonx, wonx_psb;
	int won0, won0_psb;
	while (cin >> matrix[0][0] >> matrix[0][1] >> matrix[0][2])
	{
		for (int i = 1; i<3; i++)
			for (int j = 0; j<3; j++)
				cin >> matrix[i][j];

		cntx = cnt0 = cnt_ = 0;
		wonx = won0 = 0;
		wonx_psb = won0_psb = 0;
		for (int i = 0; i<3; i++)
			for (int j = 0; j<3; j++)
			{
				if (matrix[i][j] == 'X')
					cntx++;
				else if (matrix[i][j] == '0')
					cnt0++;
				else
					cnt_++;
			}
		if (abs(cntx - cnt0) > 1)
		{
			cout << "x" << endl;
			continue;
		}
		else if (cntx > cnt0)
			wonx_psb = 1;
		else if (cnt0 > cntx)
			won0_psb = 1;
		else
		{
			wonx_psb = won0_psb = 1;
		}

		for (int i = 0; i<3; i++)
		{
			if (matrix[i][0] == matrix[i][1] && matrix[i][1] == matrix[i][2])
			{
				if (matrix[i][0] == 'X')
					wonx++;
				else if (matrix[i][0] == '0')
					won0++;
			}
		}

		for (int i = 0; i<3; i++)
		{
			if (matrix[0][i] == matrix[1][i] && matrix[1][i] == matrix[2][i])
			{
				if (matrix[0][i] == 'X')
					wonx++;
				else if(matrix[0][i] == '0')
					won0++;
			}
		}

		if (matrix[0][0] == matrix[1][1] && matrix[1][1] == matrix[2][2])
		{
			if (matrix[0][0] == 'X')
				wonx++;
			else if (matrix[0][0] == '0')
				won0++;
		}

		if (matrix[0][2] == matrix[1][1] && matrix[1][1] == matrix[2][0])
		{
			if (matrix[0][2] == 'X')
				wonx++;
			else if (matrix[0][2] == '0')
				won0++;
		}

		if (wonx && won0)
				cout << "x" << endl;
		else if (wonx)
		{
			if (wonx_psb)
			{
				if (won0_psb)
					cout << "2 won" << endl;
				else
					cout << "1 won" << endl;
			}
			else
				cout << "x" << endl;
		}
		else if (won0)
		{
			if (won0_psb)
			{
				if (wonx_psb)
					cout << "2 won" << endl;
				else
					cout << "1 won" << endl;
			}
			else
				cout << "x" << endl;
		}
		else
		{
			if (cnt_ == 0)
				cout << "draw" << endl;
			else if (cntx != cnt0)
				cout << "2" << endl;
			else
				cout << "1" << endl;
		}
	}
	return 0;
}