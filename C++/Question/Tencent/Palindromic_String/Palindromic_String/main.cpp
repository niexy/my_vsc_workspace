//腾讯2017暑期实习生编程题
//构造回文串
//给定一个字符串s，你可以从中删除一些字符，使得剩下的串是一个回文串。如何删除才能使得回文串最长呢？
//输出需要删除的字符个数。

//解法：将字符串逆序后求最大公共子序列，可使用动态规划

#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

#define MAX_LEN 1000

void rev_str(string &str, string &str_rev)
{
	str_rev = str;
	int len = str.length();

	for (int i = 0; i < len; i++)
	{
		str_rev[len - 1 - i] = str[i];
	}
}

int main()
{
	string str;
	string str_rev;
	int len = 0;
	int i, j;
	unsigned short * table = new unsigned short[(MAX_LEN + 1)*(MAX_LEN + 1)];
	for (int i = 0; i <= MAX_LEN; i++)
		table[i*MAX_LEN] = 0;
	for (int j = 0; j <= MAX_LEN; j++)
		table[j] = 0;

	while (cin >> str)
	{
		//cin >> str;
		rev_str(str, str_rev);
		len = str.length();
		for (i = 1; i <= len; i++)
			for (j = 1; j <= len; j++)
				if (str[i - 1] == str_rev[j - 1])
					table[i*MAX_LEN + j] = table[(i - 1)*MAX_LEN + j - 1] + 1;
				else
					table[i*MAX_LEN + j] = max(table[(i - 1)*MAX_LEN + j], table[i*MAX_LEN + j - 1]);

		cout << len - table[len*MAX_LEN + len] << endl;
	}

	getchar();

	return 0;
}