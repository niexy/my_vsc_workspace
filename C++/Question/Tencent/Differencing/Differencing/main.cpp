//问题：小Q今天在上厕所时想到了这个问题：有n个数，两两组成二元组，差最小的有多少对呢？差最大呢？
//解法：将数组排序，然后找出差最小和最大的对数，注意排列组合：{0, 0, 0, 0, 1}有6对差为0的数
#include<iostream>
using namespace std;

int partition(int *data, int beg, int end)
{
	int temp = data[beg];
	int i = beg, j = end;

	while (i<j)
	{
		while (data[j] >= temp && j>i)
			j--;
		if (j>i)
			data[i++] = data[j];

		while (data[i] <= temp && i<j)
			i++;
		if (i<j)
			data[j--] = data[i];
	}
	data[i] = temp;
	return i;
}

void quick_sort(int *data, int beg, int end)
{
	if (beg < end)
	{
		int pivot = partition(data, beg, end);
		quick_sort(data, beg, pivot - 1);
		quick_sort(data, pivot + 1, end);
	}
}

int main()
{
	int n;
	int max, cnt_max;
	int min, cnt_min;

	while (cin >> n)
	{
		max = 0; cnt_max = 0;
		min = 32767; cnt_min = 0;
		int *data = new int[n];
		for (int i = 0; i<n; i++)
			cin >> data[i];

		quick_sort(data, 0, n - 1);

		for (int j = n - 1; j>0; j--)
		{
			if (data[j] - data[0]<max)
				break;
			for (int i = 0; i<n; i++)
			{
				if (data[j] - data[i] >= max)
				{
					max = data[j] - data[i];
					cnt_max++;
				}
				else
					break;
			}
		}

		for (int i = 0; i + 1<n; i++)
		{
			if (data[i + 1] - data[i] < min)
			{
				min = data[i + 1] - data[i];
				cnt_min = 0;
				for (int j = i + 1; j<n && data[j] - data[i] == min; j++)
					cnt_min++;
			}
			else if (data[i + 1] - data[i] == min)
				for (int j = i + 1; j<n && data[j] - data[i] == min; j++)
					cnt_min++;
		}

		cout << cnt_min << ' ' << cnt_max << endl;

		delete[]data;
	}
	return 0;
}