#include <iostream>
using namespace std;

typedef struct user1 {
	char a;
	char a1;
	int b;
	int b1;
}user1;
typedef struct user2 {
	char c;
	user1 d;
}user2;

int main()
{
	user2 aa;
	user2 & a1 = aa;
	user2 * ap1 = &aa;

	char str[10] = "Hello!";
	char * strp1 = str;

	user2 bb[10];
	user2 (&b1)[10] = bb;
	user2 * bp1 = bb;

	int *p = new int[10];

	cout << (ap1 >= bp1) << endl;
	cout << sizeof(a1) << endl;
	cout << sizeof(ap1) << endl;
	cout << sizeof(str) << endl;
	cout << sizeof(strp1) << endl;
	cout << sizeof(bb) << endl;
	cout << sizeof(b1) << endl;
	cout << sizeof(bp1) << endl;
	cout << sizeof(p) << endl;

	char temp;
	cin >> temp;
	return 0;
}