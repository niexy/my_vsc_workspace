#include <iostream>
#include <algorithm>
using namespace std;

template <typename T>
void heap_sort(T arr[], int n, int i)
{
	if (2 * i + 2 <= n - 1)
	{
		heap_sort(arr, n, 2 * i + 1);
		heap_sort(arr, n, 2 * i + 2);
		if (arr[2 * i + 1] >= arr[2 * i + 2])
		{
			T large = max(arr[i], arr[2 * i + 1]);
			T little = min(arr[i], arr[2 * i + 1]);
			arr[i] = large;
			arr[2 * i + 1] = little;
		}
		else
		{
			T large = max(arr[i], arr[2 * i + 2]);
			T little = min(arr[i], arr[2 * i + 2]);
			arr[i] = large;
			arr[2 * i + 2] = little;
		}
	}
	else if (2 * i + 1 <= n - 1)
	{
		heap_sort(arr, n, 2 * i + 1);
		T large = max(arr[i], arr[2 * i + 1]);
		T little = min(arr[i], arr[2 * i + 1]);
		arr[i] = large;
		arr[2 * i + 1] = little;
	}
}

template <typename T>
void heap_sort(T arr[], T store[], int n)
{
	int i = 0;

	while (n > 0)
	{
		heap_sort(arr, n, 0);
		store[i] = arr[0];
		arr[0] = arr[n - 1];
		n--;
		i++;
	}
}

int main()
{
	int arr[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
	int n = sizeof(arr) / sizeof(int);
	int * store = new int[n];
	heap_sort(arr, store, n);

	for (int i = 0; i < n; i++)
	{
		cout << store[i] << ' ';
	}
	cout << endl;
	delete[] store;

	getchar();
	return 0;
}