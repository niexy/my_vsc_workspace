// myfun_test.cpp : 定义控制台应用程序的入口点。
//

#include <iostream>
#include <queue>
#include <stack>
using namespace std;
#include "nxy_btr.h"
using namespace nxy_btr;

int main()
{
	btr_node<int> * p[6] = { NULL };
	for (int i = 0; i < 6; i++)
	{
		p[i] = new btr_node<int>(i);
	}
	p[0]->lchild = p[1];
	p[0]->rchild = p[2];
	p[1]->lchild = p[3];
	p[1]->rchild = p[4];
	p[4]->lchild = p[5];

	queue<int> data_store = trav_prev(p[0],get_data);

	while (!data_store.empty())
	{
		cout << data_store.front() << endl;
		data_store.pop();
	}

	for (int i = 0; i < 6; i++)
	{
		delete p[i];
	}

	getchar();
	return 0;
}



