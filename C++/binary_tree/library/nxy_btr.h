#ifndef _NXY_BTR_
#define _NXY_BTR_

#include <queue>

namespace nxy_btr {

/***********************二叉树结点的定义*************************/
	template<typename data_T>
	class btr_node {
	private:
		data_T data;
	public:
		btr_node<data_T> * lchild;
		btr_node<data_T> * rchild;
		btr_node<data_T> * parent;
		data_T visit();
		btr_node(data_T d = 0, btr_node<data_T> *l = NULL,
			btr_node<data_T> *r = NULL, btr_node<data_T> *p = NULL) :
			data(d), lchild(l), rchild(r), parent(p) {}
		~btr_node() {}
	};

	template<typename data_T>
	data_T  btr_node<data_T>::visit() { return data; }
/****************************************************************/

/***********************二叉树先序遍历，返回存储数据序列的队列*************************/
	template <typename data_T>
	std::queue<data_T>  trav_prev(btr_node<data_T> *x)
	{
		//using std::stack;
		//using std::queue;
		std:: stack<btr_node<data_T>*> btrn_stack;
		std:: queue<data_T> data_store;

		for (;;)
		{
			while (x)        //遍历当前树/子树的左序列到底，并压右子树入栈
			{
				data_store.push(x->visit());    //读取数据
				if (x->rchild)
					btrn_stack.push(x->rchild);     //将右子树压栈
				x = x->lchild;                      //继续读取左子树
			}

			if (btrn_stack.empty())  break;      //栈被弹空，右子树被遍历完毕，完成树的遍历
			x = btrn_stack.top();       //弹出最近的右子树
			btrn_stack.pop();
		}
		return data_store;
	}
/**************************************************************************************/

}

#endif