//输入线序遍历序列和中序遍历序列，构建二叉树
#include<iostream>
#include<vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void trav_prev(TreeNode * x)
{
	if(x)
	{
		cout << x->val << endl;
		trav_prev(x->left);
		trav_prev(x->right);
	}
}

TreeNode* reConstructBinaryTree(vector<int> pre, vector<int> vin) 
{
	if (pre.empty() || vin.empty())
		return NULL;

	//if (pre.size() == 1 || vin.size() == 1)
	//{
	//	TreeNode * p = new TreeNode(*pre.begin());
	//	return p;
	//}
	
	vector<int> lchild_pre, rchild_pre, lchild_vin, rchild_vin;
	auto vin_beg = vin.begin(), vin_end = vin.end();
	auto pre_beg = pre.begin(), pre_end = pre.end();
	TreeNode * p = new TreeNode(*pre_beg);

	pre_beg++;
	while(*vin_beg != p->val && pre_beg < pre_end && vin_beg < vin_end)
	{
		lchild_vin.push_back(*vin_beg);
		lchild_pre.push_back(*pre_beg);
		vin_beg++;
		pre_beg++;
	}
	vin_beg++;
	while(pre_beg < pre_end && vin_beg < vin_end)
	{
		rchild_vin.push_back(*vin_beg);
		rchild_pre.push_back(*pre_beg);
		vin_beg++;
		pre_beg++;
	}

	p->left = reConstructBinaryTree(lchild_pre, lchild_vin);
	p->right = reConstructBinaryTree(rchild_pre, rchild_vin);

	return p;
}

int main()
{
	vector<int> pre{ 1,2,4,7,3,5,6,8 };
	vector<int> vin{ 4,7,2,1,5,3,8,6 };

	TreeNode * x= reConstructBinaryTree(pre, vin);

	trav_prev(x);

	getchar();
	return 0;
}
