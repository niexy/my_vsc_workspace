#include <iostream>
using namespace std;

class base {
private:
public:
	base() { cout << "base constructed" << endl; }
	virtual ~base() { cout << "base deconstructed" << endl; }
};
class derived: public base {
private:
public:
	derived() { cout << "derived constructed" << endl; }
	virtual ~derived() { cout << "derived deconstructed" << endl; }
};
int main()
{
	base * p = new derived;
	delete p;

	char temp;
	scanf_s("%c", &temp);
	return 0;

}