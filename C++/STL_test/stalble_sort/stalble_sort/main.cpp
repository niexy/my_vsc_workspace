#include<iostream>
#include<algorithm.>
#include<string>
using namespace std;

bool compare(char a, char b)
{
	return (b>='a' &&  a<'a') ? 1 : 0;
}

int main() 
{
	string str("BgAdFCsVc");

	stable_sort(str.begin(), str.end(), compare);

	cout << str;

	getchar();
	return 0;
}