#include<iostream>
#include<cstdlib>
using namespace std;

int compare(const void *a, const void *b)
{
	return (*(int *)a - *(int *)b);
}

int main()
{
	int data[] = { 0, 3, 2, 6, 3, 4, 6, 7, 1 };
	int size = sizeof(data)/sizeof(int);

	qsort(data, size, sizeof(int), compare);
	
	for (int i = 0; i < size; i++)
		cout << data[i] << ' ';
	cout << endl;

	getchar();
	return 0;
}